//
//  SIgnUpViewController.swift
//  InstagramLBTA
//
//  Created by Ferry Adi Wijayanto on 07/09/18.
//  Copyright © 2018 Ferry Adi Wijayanto. All rights reserved.
//

import UIKit
import Firebase

class SignUpViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //MARK:- Setup textField and Button
    
    let photoButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "plus_photo").withRenderingMode(.alwaysOriginal), for: .normal)
        button.addTarget(self, action: #selector(handleProfilePhotoPicker), for: .touchUpInside)
        return button
    }()
    
    let emailTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Email"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleFormInput), for: .editingChanged)
        return tf
    }()
    
    let UsernameTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Username"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleFormInput), for: .editingChanged)
        return tf
    }()
    
    let passwordTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Password"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.isSecureTextEntry = true
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleFormInput), for: .editingChanged)
        return tf
    }()
    
    let signUpButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Sign Up", for: .normal)
        button.backgroundColor = UIColor.rgb(r: 149, g: 204, b: 244)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 5
        button.isEnabled = false
        
        button.addTarget(self, action: #selector(handleSignUp), for: .touchUpInside)
        return button
    }()
    
    func setupSignUpForm() {
        
        view.addSubview(photoButton)
        photoButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        photoButton.anchor(top: view.safeAreaLayoutGuide.topAnchor, trailing: nil, bottom: nil, leading: nil, paddingTop: 20, paddingRight: 0, paddingBottom: 0, paddingLeft: 0, width: 140, height: 140)
        
        let stackView = UIStackView(arrangedSubviews: [emailTextField, UsernameTextField, passwordTextField, signUpButton])
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = 10
        
        view.addSubview(stackView)
        
        stackView.anchor(top: photoButton.bottomAnchor, trailing: view.safeAreaLayoutGuide.trailingAnchor, bottom: nil, leading: view.safeAreaLayoutGuide.leadingAnchor, paddingTop: 20, paddingRight: 30, paddingBottom: 0, paddingLeft: 30, width: 0, height: 200)
        
    }
    
    //MARK:- Selector functions
    
    @objc func handleFormInput() {
        // Check if all the form is filled
        let isFormValid = emailTextField.text?.characters.count ?? 0 > 0 && UsernameTextField.text?.characters.count ?? 0 > 0 && passwordTextField.text?.characters.count ?? 0 > 0
        
        if isFormValid {
            signUpButton.isEnabled = true
            signUpButton.backgroundColor = UIColor.rgb(r: 17, g: 154, b: 237)
        } else {
            signUpButton.isEnabled = false
            signUpButton.backgroundColor = UIColor.rgb(r: 149, g: 204, b: 244)
        }
    }
    
    @objc func handleProfilePhotoPicker() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        
        present(imagePickerController, animated: true, completion: nil)
    }
    
    //MARK:- Create User, save in Database, and store in Storage in Firebase
    
    @objc func handleSignUp() {
        // Create email, username, password to firebase from the input textField and check if the textField is not empty
        guard let email = emailTextField.text, email.isEmpty != true else { return }
        guard let username = UsernameTextField.text, username.isEmpty != true else { return }
        guard let password = passwordTextField.text, password.isEmpty != true else { return }
        
        // Create firebase user
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            if let err = error {
                print("Failed to create user:", err)
                return
            }
            
            print("Succesfully create user:", user?.uid ?? "")
            
            guard let image = self.photoButton.imageView?.image else { return }
            guard let uploadData = UIImageJPEGRepresentation(image, 0.3) else { return }
            
            // Create a file name based in UUID String so in Firebase storage the name in source tree can't be replaced
            let fileName = UUID().uuidString
            
            Storage.storage().reference().child("photo_images").child(fileName).putData(uploadData, metadata: nil, completion: { (metaData, error) in
                
                if let err = error {
                    print("Failed to upload images:", err)
                    return
                }
                // metaData data contain a URL
                guard let profileImageUrl = metaData?.downloadURL()?.absoluteString else { return }
                
                print("Successfully upload images:", profileImageUrl)
                
                // create a dictionary of user id and username value
                guard let uid = user?.uid else { return }
                let userDictionary = ["username": username, "profileImageUrl": profileImageUrl]
                let value = [uid : userDictionary]
                
                // Save user id in firebase database
                // In the previous journal forget to add child "users" in database
                Database.database().reference().child("users").updateChildValues(value, withCompletionBlock: { (error, reference) in
                    if let err = error {
                        print("Failed to save user in db:", err)
                        return
                    }
                    print("Successfully save user in database")
                })
            })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        setupSignUpForm()
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            photoButton.setImage(editedImage.withRenderingMode(.alwaysOriginal), for: .normal)
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            photoButton.setImage(originalImage.withRenderingMode(.alwaysOriginal), for: .normal)
        }
        
        photoButton.layer.cornerRadius = photoButton.frame.width / 2
        photoButton.layer.masksToBounds = true
        
        dismiss(animated: true, completion: nil)
    }
    
    
}












