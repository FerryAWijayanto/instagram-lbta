//
//  ProfileHeaderCell.swift
//  InstagramLBTA
//
//  Created by Ferry Adi Wijayanto on 11/09/18.
//  Copyright © 2018 Ferry Adi Wijayanto. All rights reserved.
//

import UIKit
import Firebase

class ProfileHeaderCell: UICollectionViewCell {
    
    var user: User? {
        didSet {
            fetchUserProfile()
        }
    }
    
    let profileImage: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .red
        iv.layer.cornerRadius = 80 / 2
        iv.clipsToBounds = true
        return iv
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(profileImage)
        profileImage.anchor(top: safeAreaLayoutGuide.topAnchor, trailing: nil, bottom: nil, leading: safeAreaLayoutGuide.leadingAnchor, paddingTop: 12, paddingRight: 0, paddingBottom: 0, paddingLeft: 12, width: 80, height: 80)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func fetchUserProfile() {
        
        guard let profileImageUrl = user?.profileImageUrl else { return }
        
        guard let url = URL(string: profileImageUrl) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let err = error {
                print("Failed to fetch user profile:", err)
                return
            }
            
            guard let data = data else { return }
            
            let image = UIImage(data: data)
            
            DispatchQueue.main.async {
                self.profileImage.image = image
            }
            
            }.resume()
        
    }
}












